require('dotenv').config();

const ClientModel = require('./client');
const ModuleModel = require('./module');

console.log("Starting...");
const client = new ClientModel(process.env.TYPE, process.env.URL, process.env.PORT);

const modules = [...Array(parseInt(process.env.DEVICE_NBR))].map(() => {
  return new ModuleModel();
});

//Sending telemetries
modules.map(device => {
  setInterval(
    () => {
      client.sendMetric(device.forgeTelemetry());
    },
    1000);
});

