const moment = require('moment');
const generate = require('project-name-generator');

const telemetry = require('../telemetry');
const utilities = require('../utilities');

function Module() {
  this.macAddress = utilities.getRandomMac();
  this.deviceType = utilities.getRandomDeviceType();
  this.name = `${this.deviceType}_${generate().dashed}`;
}

Module.prototype.forgeTelemetry = function() {
  return {
    name: this.name,
    macAddress: this.macAddress,
    metricDate: moment.utc().format(),
    deviceType: this.deviceType,
    metricValue: telemetry[this.deviceType](),
  };
};

module.exports = Module;

