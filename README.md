# Device simulator

## Description
This program generate sample data for a given number of devices and send them either using a REST API or MQTT. Once created, every device will send a new telemetry every second. As specified in SPRINT 1, unknown devices should be created when they send a telemetry even if they don't exist. This tool cannot be used to test device commissionning !

When using the **REST API** telemetries are sent on this endpoint:
> \<url\>/device/\<macAddress>/telemetry

The following JSON is sent, refer to the swagger file for more information
```JSON
{
  "metricDate": "2019-05-25T16:43:07Z",
  "deviceType": "gpsSensor",
  "metricValue": "N;10;8;9.14;E;18;19;9.19"
}
```

When using the MQTT mode, telemetries are sent on the MQTT sever of your choice. Every module will sent telemetries on the same topic defined in the .env file. The following JSON is sent:

```JSON
{
  "name":"humiditySensor_wealthy-snails",
  "macAddress":"44:81:C0:0D:6C:E3",
  "metricDate":"2019-05-25T16:40:13Z",
  "deviceType":"humiditySensor",
  "metricValue":"0"
}
```

## Configuration
The configuration is located in the `.env` file. You can edit each value based on your use case. There is two samples for either MQTT or REST.

- MQTT sample can be tested on: http://www.hivemq.com/demos/websocket-client/
> For MQTT, subscribe to the `telemetry` topic.
- REST sample can be tested on: https://beeceptor.com/console/iot-test-gen


## .env file structure
- URL: **required** The url (http:// or mqtt://) that will be used to send telemetries (without the route)
- PORT: **optional** The port used for MQTT
- TOPIC: **required when using MQTT** The topic used for telemetries
- TYPE: **required** `REST` or `MQTT`
- DEVICE_NBR: **required** number of devices that will be created

## Usage

1. Make sur you have Node installed (tested on 10.9.0)
2. Run `npm install`
3. Edit the `.env` file.
4. Run `node app.js`

## Help
For any inquiries please send an email to julesp@exakis.com