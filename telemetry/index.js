const utilities = require('../utilities');

const commands = {
  presenceSensor: () => utilities.getRandomInt(2).toFixed(),
  temperatureSensor: () => utilities.getRandomInt(40).toFixed(),
  brightnessSensor: () => utilities.getRandomArbitrary(0, 1000),
  atmosphericPressureSensor: () => utilities.getRandomArbitrary(0, 1000),
  humiditySensor: () => utilities.getRandomInt(0, 100).toFixed(),
  soundLevelSensor: () => utilities.getRandomInt(20, 140).toFixed(),
  gpsSensor: () => `N;${utilities.getRandomInt(50)};${utilities.getRandomInt(50)};${utilities.getRandomArbitrary(0, 50)};E;${utilities.getRandomInt(30)};${utilities.getRandomInt(30)};${utilities.getRandomArbitrary(0, 30)}`,
  co2Sensor: () => utilities.getRandomArbitrary(0, 500)
};

module.exports = commands;