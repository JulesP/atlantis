const request = require('request');
const mqtt = require('mqtt');

const sendHttp = (telemetry, url) => {
  request.post(
    `${url}/device/${telemetry.macAddress}/telemetry`,
    {
      json: true,
      body: {
        metricDate: telemetry.metricDate,
        deviceType: telemetry.deviceType,
        metricValue: telemetry.metricValue,
      }
    });
};

const sendMqtt = (telemetry, client) => {
  client.publish(process.env.TOPIC, JSON.stringify(telemetry));
}

function Client(type, url, port = null) {
  this.type = type;
  this.url = url;
  this.port = port;
  this.mqttClient = this.type === "MQTT" ? mqtt.connect(url, { port: port }) : null;
}

Client.prototype.sendMetric = function(telemetry) {
  this.type === "REST"
    ? sendHttp(telemetry, this.url)
    : sendMqtt(telemetry, this.mqttClient);
};

module.exports = Client;