const FNC = {};

const deviceTypes = [ "presenceSensor", "temperatureSensor", "brightnessSensor",
  "atmosphericPressureSensor", "humiditySensor", "soundLevelSensor",
   "gpsSensor", "co2Sensor" ];

FNC.getRandomInt = (max = 10) => {
  return Math.floor(Math.random() * Math.floor(max));
}

FNC.getRandomArbitrary = (min, max) => {
  return (Math.random() * (max - min) + min).toFixed(2);
}

FNC.getRandomMac = () => {
  return "XX:XX:XX:XX:XX:XX".replace(/X/g, function() {
    return "0123456789ABCDEF".charAt(Math.floor(Math.random() * 16))
  });
}

FNC.getRandomDeviceType = () => {
  return deviceTypes[FNC.getRandomInt(deviceTypes.length - 1)]
}

module.exports = FNC;
